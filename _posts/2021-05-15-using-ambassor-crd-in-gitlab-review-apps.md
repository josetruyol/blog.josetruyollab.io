---
layout: post
title: "Using Ambassador's CRD in GitLab Review Apps"
author: jose
categories: [GitLab, CI/CD, GitLab CI/CD, Ambassador, Kubernetes]
image: assets/images/posts/2021/05/15/title.jpg
image_credits:
  name: Andrey Metelev
  url: https://unsplash.com/@metelevan?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText
comments: true
---

As part of the DevOps cycle, our team run test per commit and per Merge Request, to catch any bug early in development. We heavily depend on [GitLab's Review Apps](https://docs.gitlab.com/ee/ci/review_apps/), and the [Kubernetes integration](https://docs.gitlab.com/ee/user/project/clusters/), for the DAST tests.

Because we use a microservice architecture on our software, and our deploys are on Kubernetes, we decided to use [ambassador](https://www.getambassador.io/) as the API gateway. At this point, we found an issue.

> Disclaimer: We are **NOT** using AutoDevOps. We have scripts to prepare and deploy and stop review apps resources, including `ambassador` Hosts and Mapping.

## The problem

If you read the Review App's docs you can see that GitLab creates a new Kubernetes Namespace where our review app will live. Inside this namespace, it [creates a new Service Account](https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html#rbac-cluster-resources) with a `RoleBinding` using `admin` as `roleRef` (you can find this in [this file](https://gitlab.com/gitlab-org/gitlab/-/blob/master/app/services/clusters/kubernetes.rb#L10)).

This Service Account has [admin rights](https://kubernetes.io/docs/reference/access-authn-authz/rbac/#user-facing-roles) inside this new namespace, but only over the default Kubernetes resources. So when our review app needs to create some Host or Mapping resources it crashes with:

```
 User "<service-account>" cannot get resource "mappings" in API group "getambassador.io" in the namespace "<namespace>"
 User "<service-account>" cannot get resource "hosts" in API group "getambassador.io" in the namespace "<namespace>"
```

Let's be sure that the service account can't create these resources:

```console
$ kubectl auth can-i create host \
  -n gitlab-review-app-namespace \
  --as=system:serviceaccount:gitlab-review-app-namespace:gitlab-review-app-service-account
no
```

Well, we can't...

> `gitlab-review-app-namespace` and `gitlab-review-app-service-account` are just placeholders. Check what values are correct in your use case

## The solution

Even if you can edit the `admin` `ClusterRole` without errors, it won't be applied. In this case, you can create a new `ClusterRole` and indicate that it should be aggregated to admin.

Saving this definition in `ambassador_cluster_role.yml`:

```yml
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
 name: aggregate-ambassador-admin
 labels:
   # Add these permissions to the "admin" default roles.
   rbac.authorization.k8s.io/aggregate-to-admin: "true"
rules:
- apiGroups:
 - getambassador.io
 resources:
 - mappings
 - hosts
 verbs:
 - create
 - delete
 - deletecollection
 - patch
 - update
 - get
 - list
 - watch
```

We can apply on the cluster with `kubectl apply -f ambassador_cluster_role.yml`

Let's check if this work:

```console
$ kubectl auth can-i create host \
  -n gitlab-review-app-namespace \
  --as=system:serviceaccount:gitlab-review-app-namespace:gitlab-review-app-service-account
yes
```

Problem solved! And now, our Review Apps can work with ambassador
