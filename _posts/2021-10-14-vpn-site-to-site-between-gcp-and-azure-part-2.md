---
layout: post
title: "VPN Site-to-Site between Azure and Google Cloud Platform. Part 2"
author: jose
categories: [VPN, GCP, Azure, Google Cloud]
image: assets/images/posts/2021/10/14/title.jpg
image_credits:
  name: Jordan Harrison
  url: https://unsplash.com/@jordanharrison?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText
comments: true
---

If you had been following along, in the [first part]({{ site.baseurl }}/vpn-site-to-site-between-gcp-and-azure/), we create a Site-to-Site VPN between Azure and GCP. Now it's time to test it

![Diagram]({{ site.baseurl }}/assets/images/posts/2021/10/14/diagram.png)

## The goal

Our goal today will be:

- Create a Virtual Machine on each cloud provider, inside the VNet used on the VPN configuration. For GCP will be the subnet `10.125.0.0/20` and for Azure will be within the address space `10.128.0.0/9`
- Check if the VM can see each other using the `ping` command using their private IPs

## Creating the Virtual Machines

We will create simple and cheap VMs with Linux as the Operating System. So, we will use only `bash` when we reach the test part.

### GCP

![Creating GCP VM]({{ site.baseurl }}/assets/images/posts/2021/10/14/gcp_001.png)
![Creating GCP VM]({{ site.baseurl }}/assets/images/posts/2021/10/14/gcp_002.png)

### Azure

![Creat GCP VM]({{ site.baseurl }}/assets/images/posts/2021/10/14/azure_001.png)

## Accessing the Virtual Machines

Now, let's open 2 new terminal windows and connect to each machine using SSH:

- GCP: `gcloud beta compute ssh --zone "us-east1-b" "vpn-test-vm" --project "vpn-test-419573"`
- Azure: `ssh -i .ssh/azure_vm_key.pem azureuser@39.125.46.15`

Next, we will find each machine IP address:

- GCP:

```console
jose@vpn-test-vm:~$ ip addr
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: ens4: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1460 qdisc mq state UP group default qlen 1000
    link/ether 42:01:0a:7d:00:02 brd ff:ff:ff:ff:ff:ff
    inet 10.125.0.2/32 brd 10.125.0.2 scope global dynamic ens4
       valid_lft 76728sec preferred_lft 76728sec
    inet6 fe80::4001:aff:fe7d:2/64 scope link
       valid_lft forever preferred_lft forever
```

- Azure:

```console
ubuntu@Gitlab:~$ ip addr
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether 00:0d:3a:02:16:98 brd ff:ff:ff:ff:ff:ff
    inet 10.128.0.4/24 brd 10.128.0.255 scope global eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::20d:3aff:fe02:1698/64 scope link
       valid_lft forever preferred_lft forever
```

Awesome, we have the private IP of the GCP VM (`10.125.0.2`) and the Azure VM (`10.128.0.4`)

## Testing the VPN

Using the private IPs we will ping each other using the `ping` command. Let's find out if the VPN is up and running.

- GCP:

```console
jose@vpn-test-vm:~$ ping 10.128.0.4 -c 4
PING 10.128.0.4 (10.128.0.4) 56(84) bytes of data.
64 bytes from 10.128.0.4: icmp_seq=1 ttl=63 time=40.3 ms
64 bytes from 10.128.0.4: icmp_seq=2 ttl=63 time=41.6 ms
64 bytes from 10.128.0.4: icmp_seq=3 ttl=63 time=42.1 ms
64 bytes from 10.128.0.4: icmp_seq=4 ttl=63 time=42.0 ms

--- 10.128.0.4 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 8ms
rtt min/avg/max/mdev = 40.268/41.482/42.059/0.734 ms
```

- Azure:

```console
ubuntu@Gitlab:~$ ping 10.125.0.2 -c 4
PING 10.125.0.2 (10.125.0.2) 56(84) bytes of data.
64 bytes from 10.125.0.2: icmp_seq=1 ttl=63 time=44.1 ms
64 bytes from 10.125.0.2: icmp_seq=2 ttl=63 time=43.2 ms
64 bytes from 10.125.0.2: icmp_seq=3 ttl=63 time=41.9 ms
64 bytes from 10.125.0.2: icmp_seq=4 ttl=63 time=41.7 ms

--- 10.125.0.2 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3003ms
rtt min/avg/max/mdev = 41.702/42.745/44.118/1.007 ms
```

Excellent! We had proven that our VPN works on both ends. But, with the current approach, we left everything open on both sides. What if we want to restrict access to only 1 target machine in the network? We can do this by providing a more narrowed address space when creating the VPN but this is a security hole.

In the next part, we will be looking for a better solution.
