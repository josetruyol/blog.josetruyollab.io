---
layout: post
title: "VPN Site-to-Site between Azure and Google Cloud Platform. Part 3"
author: jose
categories: [VPN, GCP, Azure, Google Cloud]
image: assets/images/posts/2021/10/21/title.jpg
image_credits:
  name: Jordan Harrison
  url: https://unsplash.com/@jordanharrison?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText
comments: true
---

If you had been following along, in the [first part]({{ site.baseurl }}/vpn-site-to-site-between-gcp-and-azure/), we create a Site-to-Site VPN between Azure and GCP. In the [second part]({{ site.baseurl }}/vpn-site-to-site-between-gcp-and-azure-part-2/) we tested that connection successfully. Now it's time to secure it

## Current scenario

At this point, we added 2 more VMs. Now we have 2 VMs on Azure and 2 VMs on GCP:

| VM       | Cloud Provider | Private IP   |
| -------- | -------------- | ------------ |
| GCP-01   | Google Cloud   | `10.125.0.2` |
| GCP-02   | Google Cloud   | `10.125.0.3` |
| Azure-01 | Azure          | `10.128.0.2` |
| Azure-02 | Azure          | `10.128.0.3` |

## The goal

Protect the network on Azure, allowing only specific IPs to be reached, in a maintainable way. This means that later on, we can change which IPs are whitelisted without the need to re-deploy a new VPN.

On the first test we will prevent any incoming traffic to the Azure VNet keeping the outgoing traffic to the GCP network:

| From       | To         | Can connect? |
| ---------- | ---------- | ------------ |
| `GCP-01`   | `Azure-01` | No           |
| `GCP-01`   | `Azure-02` | No           |
| `GCP-02`   | `Azure-01` | No           |
| `GCP-02`   | `Azure-02` | No           |
| `Azure-01` | `GCP-01`   | Yes          |
| `Azure-01` | `GCP-02`   | Yes          |
| `Azure-02` | `GCP-01`   | Yes          |
| `Azure-02` | `GCP-02`   | Yes          |

![Diagram]({{ site.baseurl }}/assets/images/posts/2021/10/21/diagram_01.png)

And, in the second test we will allow incoming traffic to only the Azure-01 VM:

| From       | To         | Can connect? |
| ---------- | ---------- | ------------ |
| `GCP-01`   | `Azure-01` | Yes          |
| `GCP-01`   | `Azure-02` | No           |
| `GCP-02`   | `Azure-01` | Yes          |
| `GCP-02`   | `Azure-02` | No           |
| `Azure-01` | `GCP-01`   | Yes          |
| `Azure-01` | `GCP-02`   | Yes          |
| `Azure-02` | `GCP-01`   | Yes          |
| `Azure-02` | `GCP-02`   | Yes          |

![Diagram]({{ site.baseurl }}/assets/images/posts/2021/10/21/diagram_02.png)

## How can we secure our Azure VPN?

Our Virtual Network Gateway was associated with a VNet (`default-vnet` with one subnet), and there is where we can protect our resources. We will use the `Network Security Group` (`NSG`) with some custom rules. But first, let's create the `NSG`

![Creating the Network Security Group]({{ site.baseurl }}/assets/images/posts/2021/10/21/azure_001.png)
![Creating the Network Security Group]({{ site.baseurl }}/assets/images/posts/2021/10/21/azure_002.png)

If we go to the resource overview page we can take a look into the inbound and outbound security rules

![Creating the Network Security Group]({{ site.baseurl }}/assets/images/posts/2021/10/21/azure_003.png)

Now, we go to the VNet we want to secure and in the subnet, we apply the `vpn-test-ng` Network Security Group

![Creating the inconming rule]({{ site.baseurl }}/assets/images/posts/2021/10/21/azure_004.png)

We are now ready to start to implement our rules!

## First test

In the Network Security Group, we go to the `Inbound security rules` on the sidebar and click on the `Add` button. Let's see what we need:

| Field                             | Description                                                                                                      | Value           |
| --------------------------------- | ---------------------------------------------------------------------------------------------------------------- | --------------- |
| Source                            | Specify the incoming traffic from a specific type of source                                                      | IP Addresses    |
| Source IP addresses / CIDR ranges | CIDR of the source network                                                                                       | `10.125.0.0/20` |
| Source port ranges                | From which ports this rule will take effect                                                                      | `*`             |
| Destination                       | Specify the outgoing traffic of this rule                                                                        | Any             |
| Service                           | Specify the destination protocol and port range for this rule                                                    | Custom          |
| Destination port ranges           | Ports of the destination targets                                                                                 | `*`             |
| Protocol                          | To which protocol this rule will take effect                                                                     | Any             |
| Action                            | Define if this rule will allow or deny the traffic if the conditions are met                                     | Deny            |
| Priority                          | Sets the priority of this rule. Azure will evaluate from the lowest highest priority until it found a match rule | 4000            |

We will use a high priority value because this is our fallback rule, denying everything. Later on, we can add specific rules to allow more narrowed traffic.

![Creating the inconming rule]({{ site.baseurl }}/assets/images/posts/2021/10/21/azure_005.png)

Our network should be secure from incoming traffic through the VPN. Let's check if we still have access to the GCP network:

```console
azureuser@Azure-01$ ping 10.125.0.2 -c 4
PING 10.125.0.2 (10.125.0.2) 56(84) bytes of data.
64 bytes from 10.125.0.2: icmp_seq=1 ttl=64 time=50.644 ms
64 bytes from 10.125.0.2: icmp_seq=2 ttl=64 time=48.559 ms
64 bytes from 10.125.0.2: icmp_seq=3 ttl=64 time=53.577 ms
64 bytes from 10.125.0.2: icmp_seq=4 ttl=64 time=49.720 ms

--- 10.125.0.2 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3076ms
rtt min/avg/max/mdev = 48.559/50.625/53.577/2.063 ms
```

Awesome, everything keeps working! Let's try from GCP now:

```console
ubuntu@GCP-01$ ping 10.128.0.2 -c 4
PING 10.128.0.2 (10.128.0.2) 56(84) bytes of data.
^C
--- 10.128.0.2 ping statistics ---
4 packets transmitted, 0 received, 100% packet loss, time 3057ms
ubuntu@GCP-01$ ping 10.128.0.3 -c 4
PING 10.128.0.3 (10.128.0.3) 56(84) bytes of data.
^C
--- 10.128.0.3 ping statistics ---
4 packets transmitted, 0 received, 100% packet loss, time 3053ms
```

```console
ubuntu@GCP-02$ ping 10.128.0.2 -c 4
PING 10.128.0.2 (10.128.0.2) 56(84) bytes of data.
^C
--- 10.128.0.2 ping statistics ---
4 packets transmitted, 0 received, 100% packet loss, time 3053ms
ubuntu@GCP-02$ ping 10.128.0.3 -c 4
PING 10.128.0.3 (10.128.0.3) 56(84) bytes of data.
^C
--- 10.128.0.3 ping statistics ---
4 packets transmitted, 0 received, 100% packet loss, time 3057ms
```

Excellent, our network is secure from any connection from the GCP network.

## Second test

It's time to open only the Azure-01 VM to the GCP network. This can be done by creating a new Incoming security rule:

![Creating the incoming rule]({{ site.baseurl }}/assets/images/posts/2021/10/21/azure_006.png)

In this rule, we will use the `IP Adresses` destination with Azure-01 VM as the destination IP Address and the `Allow` action. Let's apply and try it out!

```console
ubuntu@GCP-01$ ping 10.128.0.2 -c 4
PING 10.125.0.2 (10.125.0.2) 56(84) bytes of data.
64 bytes from 10.125.0.2: icmp_seq=1 ttl=64 time=50.644 ms
64 bytes from 10.125.0.2: icmp_seq=2 ttl=64 time=48.559 ms
64 bytes from 10.125.0.2: icmp_seq=3 ttl=64 time=53.577 ms
64 bytes from 10.125.0.2: icmp_seq=4 ttl=64 time=49.720 ms

--- 10.125.0.2 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3076ms
rtt min/avg/max/mdev = 48.559/50.625/53.577/2.063 ms
ubuntu@GCP-01$ ping 10.128.0.3 -c 4
PING 10.128.0.3 (10.128.0.3) 56(84) bytes of data.
^C
--- 10.128.0.3 ping statistics ---
4 packets transmitted, 0 received, 100% packet loss, time 3053ms
```

```console
ubuntu@GCP-02$ ping 10.128.0.2 -c 4-
PING 10.125.0.2 (10.125.0.2) 56(84) bytes of data.
64 bytes from 10.125.0.2: icmp_seq=1 ttl=64 time=50.644 ms
64 bytes from 10.125.0.2: icmp_seq=2 ttl=64 time=48.559 ms
64 bytes from 10.125.0.2: icmp_seq=3 ttl=64 time=53.577 ms
64 bytes from 10.125.0.2: icmp_seq=4 ttl=64 time=49.720 ms

--- 10.125.0.2 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3076ms
rtt min/avg/max/mdev = 48.559/50.625/53.577/2.063 ms
ubuntu@GCP-02$ ping 10.128.0.3 -c 4
PING 10.128.0.3 (10.128.0.3) 56(84) bytes of data.
^C
--- 10.128.0.3 ping statistics ---
4 packets transmitted, 0 received, 100% packet loss, time 3057ms
```

Awesome! We can now limit which IP addresses can be reached from the other side of the VPN, thus securing our network.

If you have a VPN device and wish to connect to the Azure VPN, you can refer to the [official documentation](https://docs.microsoft.com/en-us/azure/vpn-gateway/vpn-gateway-about-vpn-devices) and check the default values used in this VPN implementation.

Hope you found this series useful.
