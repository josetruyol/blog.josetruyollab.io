# Personal blog site

**Mediumish for Jekyll** is designed and developed by [Sal](https://www.wowthemes.net) on this [github repo](https://github.com/wowthemesnet/mediumish-theme-jekyll) and it is _free_ under MIT license.
